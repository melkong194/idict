import axios from 'axios';

const axiosInstance = axios.create({
    // baseURL: 'http://localhost:3000/',
    headers: {
        'accept': 'application/json,text/html,application/xhtml+xml,application/xml'
    }
});

export default axiosInstance;